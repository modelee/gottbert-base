# Gottbert-base

BERT model trained solely on the German portion of the OSCAR data set.

[Paper: GottBERT: a pure German Language Model](https://arxiv.org/abs/2012.02110)

Authors: Raphael Scheible, Fabian Thomczyk, Patric Tippmann, Victor Jaravine, Martin Boeker